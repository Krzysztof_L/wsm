import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

const Plant = props => {
  return (
    <View style={styles.element}>
          <View style={styles.stats}>
            <Text>{props.humidity}%</Text>
          </View>

          <View style={styles.name}>
            <Text style={styles.nameText}>{props.name}</Text>
          </View>
    </View>
  );
};

const styles = StyleSheet.create({
  element:{
    alignSelf: 'center',
    backgroundColor: '#1F1F1F',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'stretch',
    borderRadius: 40,
    height: 80,
    width: '90%',
    marginTop: 10,
  },
  name:{
    flex: 1,
    borderRadius: 40,
    alignItems:'center',
    justifyContent: 'center',
  },
  nameText:{
    color: '#FFF',
    fontSize: 25,
  },
  stats:{
    width: 80,
    backgroundColor: '#FFF',
    borderRadius: 40,
    alignItems:'center',
    justifyContent: 'center',
  },
});

export default Plant;