import { StatusBar } from 'expo-status-bar';
import React, {useState} from 'react';
import { StyleSheet, Text, View, Button, Switch, TextInput, ScrollView, FlatList } from 'react-native';
import Plant from './components/Plant'
export default function App() {
  const[addedElement, setAddedElement]= useState('');
  const[enteredIP, setEnteredIP]= useState('');
  const[elementsList, setElementList] = useState('');
  const elementInputHandler = (enteredText) => {
    setAddedElement(enteredText);
  };
  const IPInputHandler = (enteredText) => {
    setEnteredIP(enteredText);
  };
  const addElementHandler = () => {
    setElementList(currentElements => [...elementsList, {key: Math.random().toString() , id: addedElement, name: enteredIP, humidity: 0, temperature: 0}]);
    console.log(elementsList);
  };
  const [isLoading, setLoading] = useState(true);
  var refreshData = () => {
    fetch('http://192.168.2.33')
      .then((response) => response.json())
      //.then((response) => response.text())
      .then((responseData) => setElementList(responseData))
      //.then((responseData) => console.log(responseData))
      .catch((error) => console.error(error))
      .finally(() => setLoading(false));
      console.log(elementsList);
  };
  return (
    <View style={styles.screen}>
      <View style={styles.weather}>
        <Text style={styles.nameText}> Pogoda</Text>
      </View>
      <View>
      <Button title='Refresh' onPress={refreshData}/>  
      </View>
      <FlatList style={styles.list} data={elementsList} renderItem={itemData =><Plant name={itemData.item.name} humidity={itemData.item.humidity}/>}/>
    </View>
  );
}

const styles = StyleSheet.create({
  screen:{
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#151515',
  },
  weather:{
    height: '30%',
    justifyContent: 'flex-end',
    alignItems: 'center',
    paddingBottom: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#fff',
  },
  list: {
    flex: 1,
    flexDirection: 'column',
    paddingTop: 10,
  },
  
  name:{
    flex: 1,
    borderRadius: 40,
    alignItems:'center',
    justifyContent: 'center',
  },
  nameText:{
    color: '#FFF',
    fontSize: 25,
  },
  input:{
    height: 48,
    color: '#fff',
    fontSize: 20,
    textAlign: 'center',
    margin: 5,
    borderBottomColor: '#aaa',
    borderBottomWidth: 1,
  },
});
